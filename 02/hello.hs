main :: IO ()
main = do
           putStrLn s
           print (in_range 3 4 5)
    where
        s = "Hallo"
        in_range ::  Integer -> Integer -> Integer -> Bool
        in_range min max x = x >= min && x <= max
        y :: Integer
        y = 1
        in_range2 min max x =
            let inlower = ( min <= x  ) in
            let inupper = ( max >= x ) in
                inlower && inupper
