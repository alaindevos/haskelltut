import Data.Char (digitToInt)

str2int :: String -> Int
str2int [] = 0
str2int xs = digitToInt (last xs) + 10 * str2int (init xs)


isupper:: Char->Bool
isupper a = elem a ['A','B'..'Z']

myfilter:: (Char->Bool)->[Char]->[Char]
myfilter fil [] = []
myfilter fil (x:xs) =
		if (fil x) then x:(myfilter fil xs)
				   else myfilter fil xs


main = do
		 print "Geef een getal en zet om in een int:"
		 aline <-getLine
		 if null aline
			then return ()
			else do
					print aline
					print ( str2int aline)
					print "Geef een string en filter uppercase"
					aline <-getLine
					let bline=myfilter isupper aline
					print bline
					main

