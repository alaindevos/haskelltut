type Tname = String
data Tnamelist = Fnamelist { name::Tname, namelist::Tnamelist } | Niksken deriving Show

tostr::Tnamelist -> [String]
tostr Niksken = []
tostr a = (name a):(tostr (namelist a))

main = 
		let a = Niksken
		    b = Fnamelist "Alain" a
		    c = Fnamelist "Eddy"  b
		in do
			print c
			print (name c)
			print (name (namelist c))
			print (tostr c)
