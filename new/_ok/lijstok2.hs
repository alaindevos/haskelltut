type Tname = String
data Tnamelist = Fadd2list { namelist::Tnamelist , name::Tname } | Niksken deriving Show

voegtoe::String->[String]->[String]
voegtoe x y = x:y

tostr::Tnamelist -> [String]
tostr Niksken = []
tostr a = (name a) `voegtoe` (tostr (namelist a))

myprint a= do  print a

main = 
		let a = Niksken
		    b =   a `Fadd2list` "Alain" 
		    c =   b `Fadd2list` "Eddy"  
		in do
			myprint  a
			myprint  b
			myprint  c
			myprint (name c)
			myprint (name (namelist c))
			myprint (tostr c)
