module Hello where 

  import Data.List
  import System.Environment
  import System.Exit
  data Color =
      Red
    | Blue
    | Green
  data IntList = MyCons Int IntList | Nil
  data Person1 = Person1 String Int
  data Person2 = Person2 { name :: String , age ::Int } deriving (Show , Eq )
  data Color2 = RGB ( Int , Int , Int )
  type Palette = [Color2]
  data Somedata = Left Int | Right String
  data MyMaybe = ANothing | AJust Int


  main :: IO ()
  main = do
    args <-getArgs
    print "Hi"
    print p
    hw
    exitSuccess
    where
        x :: Int
        x = 1
        color2int :: Color -> Int
        color2int Red = 0
        color2int Green = 1
        color2int Blue = 2
        i :: IntList
        i2 :: IntList 
        i =  MyCons 5 Nil
        i2 = MyCons 5 (MyCons 3 Nil)
        p :: Person2
        p =  Person2 { name="Alain" , age=3 }
        i3 :: Int
        i3 = age p
        safediv :: Float -> Float -> Maybe Float
        safediv a b = if b ==0 then Nothing
                               else Just $ a / b
        fromMaybe :: Maybe Float -> Float
        fromMaybe (Just x) = x
        fromMaybe Nothing = 0
        hw  :: IO ()
        hw = do 
            putStrLn "Hello"
            putStrLn "World"
        c :: Color2
        c = RGB ( 1,2,3 )
        p2 :: Palette
        p2 = [c,c]
        mytake  _  []      =  []
        mytake  0   _      =  []
        mytake  n  (headx:tailx)  =  headx : mytake (n-1) tailx
