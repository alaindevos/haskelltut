main :: IO ()
main = do
        print (fac 5)
        print (fac2 5)
        print (is_zero 1)
    where
        fac :: Integer -> Integer
        fac n =  
            if n<= 1 then 1
                    else n*fac(n-1)
        fac2 :: Integer -> Integer
        fac2 n
            | n <= 1 = 1
            | otherwise =  n * fac (n-1)
        is_zero 0 = True
        is_zero _ = False
