import Data.List

main :: IO ()
main = do
    print l2
    print (createlist 2 5)
    print (head [1,2,3])
    print (tail [1,2,3])
    print (length [1,2,3])
    print (init [1,2,3])
    print (null [1,2,3])
    print createl1
    print (mysum [1,2,3])
    print t1
    print (addTuples (1,2) (3,4))
    where
        l1 :: [Integer]
        l1=[1,2,3,4,5]
        l2 :: [Integer]
        l2=0 : l1
        createlist :: Int -> Int -> [ Int ]
        createlist n m
            | m < n = []
            | m ==n = [m]
            | m > n = n : createlist (n+1) m 
        createl1 = [2 * x | x <- [1,2,3,4] , x > 2]
        mysum :: [ Int ] -> Int
        mysum [] = 0
        mysum (x:xs) = x + mysum xs
        t1 = (1,2) :: (Int,Int)
        addTuples ::  (Int,Int) -> (Int,Int) -> (Int,Int)
        addTuples (x1,y1) (x2,y2) = (x1+x2,y1+y2)
        check_elem_in_list _ [] = False
        check_elem_in_list e (x:xs) = ( e==x ) || (elem e xs)
        add1 :: Int -> Int
        add1 x = x + 1
        app :: ( a->b ) -> a -> b
        app f x = f x
        anum = app add1 1 
